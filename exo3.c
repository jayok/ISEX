#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>

/*Init nombre père et fils*/
int nbP = 0;
int nbF = -1;

/*pid père et fils*/
pid_t pere;
pid_t fils;

void f_pere()
{
  nbP += 2;
  if ( nbP <= 100)
    printf("p :%d\n", nbP);
  kill(fils, SIGUSR1);
}

void f_fils()
{
  nbF += 2;
  if ( nbF <= 99)
    printf("f %d\n", nbF);
  kill(pere, SIGUSR1);
}

int main()
{

  pere = getpid();
  fils = fork();
  if(fils != 0)
  {
    signal(SIGUSR1,f_pere);
    sleep(1);
    kill(fils, SIGUSR1);
    while(nbP < 100)
    {
      pause();
    }
  }  
  else
  {
    signal(SIGUSR1, f_fils);
    while(nbF < 99)
    {
      pause();
    }
  }
  return 0;
}
