#include<stdio.h>
#include <stdlib.h>
#include<signal.h>
#include<unistd.h>
int count = 0;
void sig_handler(int signo)
{
	count += 1;
	printf("Received #%d Ctrl-C\n", count);
	if(count == 5)
	{
		exit(0);
	}
}

int main(void)
{
	signal(SIGINT, sig_handler);
  	// A long long wait so that we can easily issue a signal to this process
	while(1) 
		sleep(1);
	return 0;
}