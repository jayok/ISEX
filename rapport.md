# TP 4 ISEX

 * O'Keeffe Jérôme
 * Roux Romain

## Exercice 1

J'ai utilisé une boucle sur tous les signaux qui utilisent un même handler.

```C
for (int sig = 1; sig < NSIG; sig++)
{
	signal(sig, handle_signal);
}
```

Ensuite j'utilise juste le signal passé en paramètre pour afficher le signal correspondant. J'ai stocké l'ensemble des signaux dans un tableau.

```C
char *signame[]={"INVALID", "SIGHUP", "SIGINT", "SIGQUIT", "SIGILL", "SIGTRAP", "SIGABRT", "SIGBUS", "SIGFPE", "SIGKILL", "SIGUSR1", "SIGSEGV", "SIGUSR2", "SIGPIPE", "SIGALRM", "SIGTERM", "SIGSTKFLT", "SIGCHLD", "SIGCONT", "SIGSTOP", "SIGTSTP", "SIGTTIN", "SIGTTOU", "SIGURG", "SIGXCPU", "SIGXFSZ", "SIGVTALRM", "SIGPROF", "SIGWINCH", "SIGPOLL", "SIGPWR", "SIGSYS", NULL};

void upcase(char *s)
{
    while (*s)
    {
        *s = toupper(*s);
        s++;        
    }
}

void handle_signal(int signal) {
	char *str = strdup(signame[signal]);
    upcase(str);
    printf("%2d -> SIG%s\n", signal, str);
    free(str);
}
```	


![Execution](images/exo1.png)

## Exercice 2

Pour trap le Ctrl-C, j'utilise ```signal(SIGINT, sig_handler);```. Ensuite j'utilise une variable globale qui permet de déterminer à quel moment j'effectue un exit(0).

![Execution](images/exo2.png)


## Exercice 3

Une alternance fils/père avec un signal suffit à la suite de l'affichage du père et à la suite de l'affichage du fils.

```C
pere = getpid();
  fils = fork();
  if(fils != 0)
  {
    signal(SIGUSR1,f_pere);
    sleep(1);
    kill(fils, SIGUSR1);
    while(nbP < 100)
    {
      pause();
    }
  }  
  else
  {
    signal(SIGUSR1, f_fils);
    while(nbF < 99)
    {
      pause();
    }
  }
 ```

![Execution](images/exo3.png)
